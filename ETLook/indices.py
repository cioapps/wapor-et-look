import numpy as np


def normalized_difference_index(b1, b2):
    r"""Calculate the Normalized Difference Index.

    eq 15.2 from Gitelson (2015)

    .. math ::
        ndi=\frac{\rho_{b2}-\rho_{b1}}{\rho_{b2}+\rho_{b1}}

    Parameters
    ----------
    b1 : float
        input band 1
        :math:`\rho_{b1}`
        [-]
    b2 : float
        input band 2
        :math:`\rho_{b2}`
        [-]

    Returns
    -------
    ndi : float
        Normalized Difference Index
        :math:`ndi`
        [-]

    """

    n1 = b2 - b1
    n2 = b2 + b1

    return np.where(b2 == 0, 0, n1 / n2)


def normalized_difference_vegetation_index(red, nir):
    r"""Calculate the Normalized Difference Vegetation Index.

    https://www.indexdatabase.de/db/i-single.php?id=58

    .. math ::
       NDVI_{r} = \dfrac{\rho_{NIR}-\rho_{r}}{\rho_{NIR}+\rho_{r}}

    Parameters
    ----------
    red : float
        red reflectance
        :math:`\rho_{r}`
        [-]
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]

    Returns
    -------
    ndvi : float
        Normalized Difference Vegetation Index
        :math:`NDVI_{r}`
        [-]

    """

    return normalized_difference_index(red, nir)


def normalized_difference_vegetation_index_green(green, nir):
    r"""Calculate the Normalized Difference Vegetation Index.

    https://www.indexdatabase.de/db/i-single.php?id=401

    .. math ::
        NDVI_{g} = \dfrac{\rho_{NIR}-\rho_{g}}{\rho_{NIR}+\rho_{g}}

    Parameters
    ----------
    green : float
        green reflectance
        :math:`\rho_{g}`
        [-]
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]

    Returns
    -------
    ndvi_green : float
        Normalized Difference Vegetation Index (Green)
        :math:`NDVI_{g}`
        [-]

    """

    return normalized_difference_index(green, nir)


def normalized_difference_vegetation_index_red_edge(red_edge, nir):
    r"""Calculate the Normalized Difference Vegetation Index.

    https://www.indexdatabase.de/db/i-single.php?id=223

    .. math ::
        NDVI_{red\_edge} = \dfrac{\rho_{NIR}-\rho_{red\_edge}}{\rho_{NIR}+\rho_{red\_edge}}

    Parameters
    ----------
    red_edge : float
        red_edge reflectance
        :math:`\rho_{red\_edge}`
        [-]
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]

    Returns
    -------
    ndvi_red_edge : float
        Normalized Difference Vegetation Index (Red Edge)
        :math:`NDVI_{red\_edge}`
        [-]

    """

    return normalized_difference_index(red_edge, nir)


def normalized_difference_moisture_index(swir1, nir):
    r"""Calculate the Normalized Difference Moisture Index.

    nir = 820nm
    swir1 = 1600nm

    https://www.indexdatabase.de/db/i-single.php?id=56

    .. math ::
        ndmi = \dfrac{\rho_{NIR}-\rho_{SWIR1}}{\rho_{NIR}+\rho_{SWIR1}}

    Parameters
    ----------
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]
    swir1 : float
        siw1 reflectance (1600nm)
        :math:`\rho_{SWIR1}`
        [-]

    Returns
    -------
    ndmi : float
        Normalized Difference Moisture Index
        :math:`ndmi`
        [-]

    """

    return normalized_difference_index(swir1, nir)


def normalized_difference_snow_index(swir1, green):
    r"""Calculate the Normalized Difference Snow Index.

    green = 560nm
    swir1 = 1600nm

    https://sentinels.copernicus.eu/web/sentinel/technical-guides/sentinel-2-msi/level-2a/algorithm

    .. math ::
        ndsi = \dfrac{\rho_{g}-\rho_{SWIR1}}{\rho_{g}+\rho_{SWIR1}}


    Parameters
    ----------
    green : float
        green reflectance
        :math:`\rho_{G}`
        [-]
    swir1 : float
        swir1 reflectance (1600nm)
        :math:`\rho_{SWIR1}`
        [-]

    Returns
    -------
    ndsi : float
        Normalized Difference Snow Index
        :math:`NDSI`
        [-]

    """

    return normalized_difference_index(swir1, green)


def enhanced_vegation_index(
    blue, red, nir, gain_factor=2.5, c1=6.0, c2=7.5, l_coeff=1.0
):
    r"""Calculate the Enhanced Vegetation Index. The parameters related to atmopsheric
    inlfuence imply an imperfect atmospheric correction of the input bands.

    https://en.wikipedia.org/wiki/Enhanced_vegetation_index

    https://www.indexdatabase.de/db/i-single.php?id=16

    .. math ::
        EVI = G\dfrac{\rho_{NIR}-\rho_{R}}{\rho_{NIR}+C_1\rho_R-C_2\rho_B+L}

    Parameters
    ----------
    blue : float
        blue reflectance
        :math:`\rho_{B}`
        [-]
    red : float
        red reflectance
        :math:`\rho_{R}`
        [-]
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]
    gain_factor : float
        gain_factor related to canopy background
        :math:`G`
        [-]
    c1 : float
        c1 coefficient related to aerosol influence
        :math:`\c_1`
        [-]
    c2 : float
        c2 coefficient related to aerosol influence
        :math:`\c_2`
        [-]
    l_coeff : float
        l_coeff
        :math:`\l{coeff}`
        [-]

    Returns
    -------
    evi : float
        Enhanced Vegetation Index
        :math:`EVI`
        [-]

    """

    n1 = nir - red
    n2 = nir + c1 * red - c2 * blue + l_coeff

    evi = np.where(n2 == 0, 0, gain_factor * (n1 / n2))

    return evi


def enhanced_vegation_index2(red, nir, gain_factor=2.5, c1=2.4, l_coeff=1.0):
    r"""Calculate the two-band Enhanced Vegetation Index (EVI2).

    eq 15.3 from Gitelson (2015)

    https://www.indexdatabase.de/db/i-single.php?id=576

    https://en.wikipedia.org/wiki/Enhanced_vegetation_index#Two-band_EVI

    .. math ::
        EVI2 = G\frac{\rho_{NIR}-\rho_{R}}{\rho_{NIR}+C_1\rho_{R}+L}

    Parameters
    ----------
    red : float
        red reflectance
        :math:`\rho_{R}`
        [-]
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]
    gain_factor : float
        gain_factor related to canopy background
        :math:`G`
        [-]
    c1 : float
        c1 coefficient related to aerosol influence
        :math:`\c_1`
        [-]
    l_coeff : float
        l_coeff
        :math:`\l{coeff}`
        [-]

    Returns
    -------
    evi2 : float
        Two-Band Enhanced Vegetation Index
        :math:`EVI_2`
        [-]

    """

    n1 = nir - red
    n2 = nir + c1 * red + l_coeff

    evi = np.where(n2 == 0, 0, gain_factor * (n1 / n2))

    return evi


def wide_dynamic_range_vegetation_index(red, nir, alpha=0.1):
    r"""Calculate the Wide Dynamic Range Vegetation Index (WDRVI)

    eq 15.4 from Gitelson (2015)

    https://www.indexdatabase.de/db/i-single.php?id=125

    .. math ::
        wdrvi=\dfrac{\alpha\rho_{NIR}-\rho_{R}}{\alpha\rho_{NIR}+\rho_{R}}

    Parameters
    ----------
    red : float
        red reflectance
        :math:`\rho_{R}`
        [-]
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]
    alpha : float
        alpha factor to attenuate nir contribution at high biomass
        :math:`\alpha`
        [-]

    Returns
    -------
    wdrvi : float
         Wide Dynamic Range Vegetation Index
        :math:`WDRVI`
        [-]
    """

    return normalized_difference_index(red, nir * alpha)


def visible_atmospherically_resistant_index_green(blue, green, red):
    r"""Calculate the Visible Atmospherically Resistant Index Green (VARI Green).

    eq 15.5 from Gitelson (2015)

    https://www.indexdatabase.de/db/i-single.php?id=356

    .. math ::
        VARI_{green} = \frac{\rho_{G}-\rho_{R}}  {\rho_{G}+\rho_{R}-\rho_{B}}

    Parameters
    ----------
    blue : float
        blue reflectance
        :math:`\rho_{B}`
        [-]
    green : float
        green reflectance
        :math:`\rho_{G}`
        [-]
    red : float
        red reflectance
        :math:`\rho_{R}`
        [-]

    Returns
    -------
    vari_green : float
        Visible Atmospherically Resistant Index Green
        :math:`VARI_{green}`
        [-]
    """

    n1 = green - red
    n2 = green + red - blue

    vari = np.where(n2 == 0, 0, (n1 / n2))

    return vari


def visible_atmospherically_resistant_index_red_edge(blue, red_edge, red):
    r"""Calculate the Visible Atmospherically Resistant Index Red Edge (VARI Red Edge).

    eq 15.6 from Gitelson (2015)

    https://www.indexdatabase.de/db/i-single.php?id=130

    .. math ::
        VARI_{red\_edge} = \frac{\rho_{red\_edge}-1.7\rho_{R}+0.7\rho_{B}}{\rho_{red\_edge}+2.3\rho_{R}-1.3\rho_{B}}

    Parameters
    ----------
    blue : float
        blue reflectance
        :math:`\rho_{B}`
        [-]
    red_edge : float
        red_edge reflectance
        :math:`\rho_{red\_edge}`
        [-]
    red : float
        red reflectance
        :math:`\rho_{R}`
        [-]

    Returns
    -------
    vari_red_edge : float
        Visible Atmospherically Resistant Index Green
        :math:`VARI_{red\_edge}`
        [-]
    """

    n1 = red_edge - 1.7 * red + 0.7 * blue
    n2 = red_edge + 2.3 * red - 1.3 * blue

    vari = np.where(n2 == 0, 0, (n1 / n2))

    return vari


def modified_chlorophyll_absorption_ratio_index(green, red, red_edge):
    r"""Calculate the Modified Chlorophyll Absorption Ratio Index (MCARI)

    green should be at 550nm (band 3 of S2 MSI, 665nm)
    red should be at 670nm (band 4 of S2 MSI, 665nm)
    red_edge should be at 700nm (band 5 of S2 MSI, 705nm)

    eq 15.10 from Gitelson (2015)

    https://www.indexdatabase.de/db/i-single.php?id=41

    .. math ::
        MCARI = ((\rho_{red\_edge}-\rho_{R}) - 0.2(\rho_{red\_edge}-\rho_{G}))\dfrac{\rho_{red_edge}}{\rho_{R}}

    Parameters
    ----------
    green : float
        green reflectance
        :math:`\rho_{G}`
        [-]
    red : float
        red reflectance
        :math:`\rho_{R}`
        [-]
    red_edge : float
        red_edge reflectance
        :math:`\rho_{red\_edge}`
        [-]

    Returns
    -------
    mcari : float
        Modified Chlorophyll Absorption Ratio Index (MCARI)
        :math:`MCARI`
        [-]
    """

    p1 = red_edge - red
    p2 = np.where(red == 0, 0, 0.2 * (red_edge - green) * (red_edge / red))

    mcari = p1 - p2

    return mcari


def optimized_soil_adjusted_vegetation_index(red, nir, y=0.16):
    r"""Calculate the Optimized Soil Adjusted Vegetation Index (OSAVI)

    eq 15.11 from Gitelson (2015)

    https://www.indexdatabase.de/db/i-single.php?id=63

    .. math ::
        OSAVI = (1 + Y)\frac{\rho_{NIR}-\rho_{R}}{\rho_{NIR}+\rho_{R}+Y}

    Parameters
    ----------
    red : float
        red reflectance
        :math:`\rho_{R}`
        [-]
    nir : float
        nir reflectance
        :math:`\rho_{NIR}`
        [-]
    y : float
        osavi parameter
        :math:`Y`
        [-]

    Returns
    -------
    osavi : float
        Optimized Soil Adjusted Vegetation Index (OSAVI)
        :math:`OSAVI`
        [-]
    """

    n1 = (1 + y) * (nir - red)
    n2 = nir + red + y

    osavi = np.where(n2 == 0, 0, (n1 / n2))

    return osavi


def chlorophyll_index(b1, b2):
    r"""Calculate the Chlorophyll Index (CI).

    .. math ::
        ci = \frac{\rho_{b2}}{\rho_{b1}}-1

    Parameters
    ----------
    b1 : float
        input band 1
        :math:`\rho_{b1}`
        [-]
    b2 : float
        input band 2
        :math:`\rho_{b2}`
        [-]

    Returns
    -------
    ci : float
        Chlorophyll Index (CI)
        :math:`ci`
        [-]

    """

    return np.where(b1 == 0, 0, (b2 / b1) - 1)


def chlorophyll_index_green(green, nir):
    r"""Calculate the Chlorophyll Index (CI).

    eq 15.13 from Gitelson (2015)

    https://www.indexdatabase.de/db/i-single.php?id=128

    .. math ::
        ci = \frac{\rho_{NIR}}{\rho_{G}}-1

    Parameters
    ----------
    green : float
        green reflectance
        :math:`\rho_{G}`
        [-]
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]

    Returns
    -------
    ci_g : float
        Chlorophyll Index Green (CI Green)
        :math:`ci_{green}`
        [-]
    """

    return chlorophyll_index(green, nir)


def chlorophyll_index_red_edge(red_edge, nir):
    r"""Calculate the Chlorophyll Index (CI).

    eq 15.14 from Gitelson (2015)

    https://www.indexdatabase.de/db/i-single.php?id=131

    .. math ::
        ci = \frac{\rho_{NIR}}{\rho_{red\_edge}}-1

    Parameters
    ----------
    red_edge : float
        red_edge reflectance
        :math:`\rho_{red\_edge}`
        [-]
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]

    Returns
    -------
    ci_re : float
        Chlorophyll Index Red Edge (CI Red Edge)
        :math:`ci_{red\_edge}`
        [-]

    """

    return chlorophyll_index(red_edge, nir)


def modified_simple_ratio(b1, b2):
    r"""Calculate the Modified Simple Ratio (MSR).

    # table 2 from Dong et al (2019)

    https://www.indexdatabase.de/db/i-single.php?id=362

    .. math ::
        msr = \frac{\frac{\rho_{b2}}{\rho_{b1}}-1}{\sqrt{\frac{\rho_{b2}}{\rho_{b1}}+1}}

    Parameters
    ----------
    b1 : float
        input band 1
        :math:`\rho_{b1}`
        [-]
    b2 : float
        input band 2
        :math:`\rho_{b2}`
        [-]

    Returns
    -------
    msr : float
        Modified Simple Ratio (MSR)
        :math:`msr_{red\_edge}`
        [-]
    """

    r = b2 / b1
    msr = (r - 1) / np.sqrt(r + 1)

    return np.where(b1 == 0, 0, msr)


def modified_simple_ratio_red_edge(red_edge, nir):
    r"""Calculate the Modified Simple Ratio (MSR Red Edge).

    # table 2 from Dong et al (2019)

    https://www.indexdatabase.de/db/i-single.php?id=362

    .. math ::
        msr_{red\_edge} = \frac{\frac{\rho_{NIR}}{\rho_{red\_edge}}-1}{\sqrt{\frac{\rho_{NIR}}{\rho_{red\_edge}}+1}}

    Parameters
    ----------
    red_edge : float
        red_edge reflectance
        :math:`\rho_{red\_edge}`
        [-]
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]

    Returns
    -------
    msr_red_edge : float
        Modified Simple Ratio (MSR Red Edge)
        :math:`msr_{red\_edge}`
        [-]

    """

    return modified_simple_ratio(red_edge, nir)


def plant_senescence_reflectance_index(blue, red, red_edge):
    r"""Calculate the Plant Senescence Reflectance Index.

    https://www.indexdatabase.de/db/i-single.php?id=69

    .. math ::
            PSRI = \dfrac{\rho_{R}-\rho_{B}}{\rho_{red\_edge}}



    Parameters
    ----------
    red_edge : float
        red_edge reflectance
        :math:`\rho_{red\_edge}`
        [-]
    red : float
        red reflectance
        :math:`\rho_{R}`
        [-]
    blue : float
        blue reflectance
        :math:`\rho_{B}`
        [-]

    Returns
    -------
    psri : float
        Plant Senescence Reflectance Index (PSRI)
        :math:`PSRI`
        [-]

    """
    return np.where(red_edge == 0, 0, (red-blue) / red_edge)


def leaf_water_content_index(nir, midir):
    r"""Calculate the Plant Senescence Reflectance Index.

    https://www.indexdatabase.de/db/i-single.php?id=129

    Definition at the website seems to be always -1.

    .. math ::
            LWCI = \dfrac{\ln [1-(\rho_{NIR}-\rho_{MIDIR})]}{-\ln [1-(\rho_{MIDIR}-0.2)]}


    Parameters
    ----------
    nir : float
        red_edge reflectance
        :math:`\rho_{NIR}`
        [-]
    midir : float
        mid infrared reflectance
        :math:`\rho_{MIDIR}`
        [-]

    Returns
    -------
    lwci : float
        Leaf Water Content Index (LWCI)
        :math:`LWCI`
        [-]

    """
    n1 = -np.log(1-(nir-midir))
    n2 = -np.log(1-(nir-0.2))

    return np.where(n2 == 0, 0, n1/n2)


def moisture_stress_index(swir1, nir):
    r"""Calculate the moisture stress index.

    https://www.indexdatabase.de/db/i-single.php?id=48

    .. math ::
            MSI = \dfrac{\rho_{SWIR1}}{\rho_{NIR}}


    Parameters
    ----------
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]
    swir1 : float
        swir1 reflectance (1600nm)
        :math:`\rho_{SWIR1}`
        [-]

    Returns
    -------
    msi : float
        Moisture Stress Index
        :math:`MSI`
        [-]

    """
    return np.where(nir == 0, 0, swir1 / nir)


def global_vegetation_moisture_index(swir1, nir):
    r"""Calculate the global vegetation moisture index

    https://www.indexdatabase.de/db/i-single.php?id=372

    .. math ::
        GVMI = \dfrac{(\rho_{NIR}+0.1)-(\rho_{SWIR1}+0.02)}{(\rho_{NIR}+0.1)+(\rho_{SWIR1}+0.02)}


    Parameters
    ----------
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]
    swir1 : float
        swir1 reflectance (1600nm)
        :math:`\rho_{SWIR1}`
        [-]

    Returns
    -------
    gvmi : float
        Moisture Stress Index
        :math:`GVMI`
        [-]

    """
    b2 = nir+0.1
    b1 = swir1+0.02

    return normalized_difference_index(b1, b2)


def normalized_difference_water_index(nir, green):
    r"""Calculate the Normalized Difference Moisture Index.

    https://www.indexdatabase.de/db/i-single.php?id=60

    .. math ::
        ndwi = \dfrac{\rho_{G}-\rho_{NIR}}{\rho_{G}+\rho_{NIR}}

    Parameters
    ----------
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]
    green : float
        green reflectance
        :math:`\rho_{G}`
        [-]

    Returns
    -------
    ndwi : float
        Normalized Difference Water Index
        :math:`ndwi`
        [-]

    """

    return normalized_difference_index(nir, green)


def visible_and_shortwave_infrared_drought_index(blue, red, swir1):
    r"""Calculate the Visible and Shortwave Infrared Drought Index

    For a description of the index see: https://doi.org/10.1080/01431161.2013.779046

    .. math ::
        VSDI = 1-[(\rho_{SWIR}-\rho_{B})+(\rho_{R}-\rho_{B})]

    Parameters
    ----------
    blue : float
        blue reflectance
        :math:`\rho_{B}`
        [-]
    red : float
        red reflectance
        :math:`\rho_{R}`
        [-]
    swir1 : float
        swir1 reflectance
        :math:`\rho_{SWIR\_1}`
        [-]

    Returns
    -------
    VSDI : float
        Visible and Shortwave infrared Drought Index
        :math:`VSDI`
        [-]

    """
    return 1-((swir1-blue)+(red-blue))


def surface_water_capacity_index(swir2, swir1):
    r"""Calculate the Surface Water Capacity Index.

    From Du, X. et al., 2007, Construction and Validation of a new model for unified
    surface water capacity based on MODIS data. Geomatics and Information Science
    of Wuhan University 32(3):205-207 (in Chinese)

    .. math ::
        SWCI = \dfrac{\rho_{SWIR\_1}-\rho_{SWIR\_2}}{\rho_{SWIR\_1}+\rho_{SWIR\_2}}

    Parameters
    ----------
    swir1 : float
        shortwave infrared reflectance (1600nm)
        :math:`\rho_{SWIR\_1}`
        [-]
    swir2 : float
        shortwave infrared reflectance (2100nm)
        :math:`\rho_{SWIR\_2}`
        [-]

    Returns
    -------
    swci : float
        Surface Water Capacity Index
        :math:`SWCI`
        [-]

    """

    return normalized_difference_index(swir2, swir1)


def shortwave_infrared_soil_moisture_index(swir2, swir1):
    r"""Calculate the Shortwave Infrared soil Moisture Index (SIMI).

    Introduced by Yao et al. 2011: retrieval of Soil Moisture
       Based on MODIS Shortwave Infrared Spectral Feature. Journal of
       Infrared and Millimeter Waves 30 (1): 61–6

    .. math ::
        SIMI = \sqrt{\dfrac{\rho_{SWIR\_1}^2+\rho_{SWIR\_2}^2}{2}}

    Parameters
    ----------
    swir1 : float
        shortwave infrared reflectance (1600nm)
        :math:`\rho_{SWIR\_1}`
        [-]
    swir2 : float
        shortwave infrared reflectance (2100nm)
        :math:`\rho_{SWIR\_2}`
        [-]

    Returns
    -------
    simi : float
        Shortwave Infrared soil Moisture Index
        :math:`SIMI`
        [-]

    """
    return np.sqrt((swir1**2+swir2**2)/2)


def normalized_multiband_drought_index(swir2, swir1, nir):
    r"""Calculate the Normalized Multiband Drought Index

    nir = 820nm
    swir1 = 1600nm
    swir2 = 2100 nm

    see Wang and Qu (2007): https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2007GL031021

    .. math ::
        NMDI = \dfrac{\rho_{NIR}-(\rho_{SWIR\_1}-\rho_{SWIR\_2})}{\rho_{NIR}+(\rho_{SWIR\_1}-\rho_{SWIR\_2})}


    Parameters
    ----------
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]
    swir1 : float
        swir reflectance (1600nm)
        :math:`\rho_{SWIR\_1}`
        [-]
    swir2 : float
        swir reflectance (2100nm)
        :math:`\rho_{SWIR\_2}`
        [-]

    Returns
    -------
    nmdi : float
        Normalized Multiband Drought Index
        :math:`nmdi`
        [-]

    """
    return normalized_difference_index(swir1-swir2, nir)


def soil_adjusted_vegetation_index(red, nir):
    r"""Calculate the Soil Adjusted Vegetation Index (SAVI)

    https://www.indexdatabase.de/db/i-single.php?id=87

    .. math ::
        SAVI = \dfrac{(1+L)(\rho_{NIR}-\rho_{R})}{\rho_{NIR}+\rho_{R}+L}


    Parameters
    ----------
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]
    red : float
        red reflectance
        :math:`\rho_{R}`
        [-]

    Returns
    -------
    savi : float
        Soil Adjusted Vegetation Index
        :math:`SAVI`
        [-]

    """
    l = 0.5
    n1 = (1+l)*(nir-red)
    n2 = nir+red+l
    return np.where(n2 == 0, 0, n1/n2)


def modified_soil_adjusted_vegetation_index(red, nir):
    r"""Calculate the Soil Adjusted Vegetation Index (SAVI)

    https://www.indexdatabase.de/db/i-single.php?id=44

    .. math ::
        MSAVI = 0.5*((2\rho_{NIR}+1)-\sqrt{(2\rho_{NIR}+1)^2-8(\rho_{NIR}-\rho_R)})

    Parameters
    ----------
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]
    red : float
        red reflectance
        :math:`\rho_{R}`
        [-]

    Returns
    -------
    msavi : float
        Modified Soil Adjusted Vegetation Index
        :math:`MSAVI`
        [-]

    """
    n1 = 2*nir+1
    test = n1**2-8*(nir-red)
    return np.where(test < 0, 0, 0.5*n1-np.sqrt(test))


def normalized_difference_built_up_index(swir1, nir):
    r"""Calculate the Normalized Difference Built-up Index.

    .. math ::
       NDVI_{r} = \dfrac{\rho_{SWIR\_1}-\rho_{NIR}}{\rho_{SWIR\_1}+\rho_{NIR}}

    Parameters
    ----------
    swir1 : float
        swir reflectance (1600nm)
        :math:`\rho_{SWIR\_1}`
        [-]
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]

    Returns
    -------
    ndbi : float
        Normalized Difference Built-up Index
        :math:`NDBI`
        [-]

    """

    return normalized_difference_index(nir, swir1)


def modified_normalized_difference_water_index(swir1, green):
    r"""Calculate the Normalized Difference Built-up Index.

    .. math ::
       MNDWI = \dfrac{\rho_{G}-\rho_{SWIR\_1}{\rho_{G}+\rho_{SWIR\_1}}

    Parameters
    ----------
    swir1 : float
        swir reflectance (1600nm)
        :math:`\rho_{SWIR\_1}`
        [-]
    green : float
        green reflectance
        :math:`\rho_{G}`
        [-]

    Returns
    -------
    mndwi : float
        Modified Normalized Difference Water Index
        :math:`MNDWI`
        [-]

    """

    return normalized_difference_index(swir1, green)


def bare_soil_index(nir, swir1, red, blue):
    r"""Calculate the bare soil index BSI

    .. math ::
       BSI = \dfrac{(\rho_{SWIR\_1}+\rho_R)-(\rho_{NIR}+\rho_{B})}{(\rho_{SWIR\_1}+\rho_R)+(\rho_{NIR}+\rho_{B})}


    Parameters
    ----------
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]
    swir1 : float
        swir reflectance (1600nm)
        :math:`\rho_{SWIR\_1}`
        [-]
    red : float
        red reflectance
        :math:`\rho_{R}`
        [-]
    blue : float
        blue reflectance
        :math:`\rho_{B}`
        [-]

    Returns
    -------
    bsi : float
        Bare Soil Index
        :math:`BSI`
        [-]

    """
    return normalized_difference_index(nir+blue, swir1+red)


def bare_index(nir, swir2, red, blue):
    r"""Calculate the bare index BI

    .. math ::
       BI = \dfrac{(\rho_{SWIR\_2}+\rho_R)-(\rho_{NIR}+\rho_{B})}{(\rho_{SWIR\_2}+\rho_R)+(\rho_{NIR}+\rho_{B})}


    Parameters
    ----------
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]
    swir2 : float
        swir reflectance (1600nm)
        :math:`\rho_{SWIR\_2}`
        [-]
    red : float
        red reflectance
        :math:`\rho_{R}`
        [-]
    blue : float
        blue reflectance
        :math:`\rho_{B}`
        [-]

    Returns
    -------
    bi : float
        Bare Soil Index
        :math:`BSI`
        [-]

    """
    return normalized_difference_index(nir+blue, swir2+red)


def normalized_difference_drought_index(ndvi, ndwi):
    r"""Calculate the normalized difference drought index

    .. math ::
       NDDI = \dfrac{NDVI_r-NDWI}{NDVI_r+NDWI}


    Parameters
    ----------
    ndvi : float
        Normalized Difference Vegetation Index
        :math:`NDVI_{r}`
        [-]
    ndwi : float
        Normalized Difference Water Index
        :math:`NDWI`
        [-]

    Returns
    -------
    nddi : float
        Normalized Difference Drought Index
        :math:`NDDI`
        [-]

    """
    return normalized_difference_index(ndwi, ndvi)


def index_based_built_up_index(ndbi, savi, mndwi):
    r"""Calculate the Index based Built-up Index IBI

    .. math ::
       IBI = \dfrac{NDBI-(SAVI+MNDWI)/2}{NDBI+(SAVI+MNDWI)/2}


    Parameters
    ----------
    ndbi : float
        Normalized Difference Built-up Index
        :math:`NDBI`
        [-]
    mndwi : float
        Modified Normalized Difference Water Index
        :math:`MNDWI`
        [-]
    savi : float
        Soil Adjusted Vegetation index
        :math:`SAVI`
        [-]

    Returns
    -------
    ibi : float
        Index based Built-up Index
        :math:`IBI`
        [-]

    """
    return normalized_difference_index((savi+mndwi)/2, ndbi)


def index_based_vegetation_index(ndbi, savi, mndwi):
    r"""Calculate the Index based Vegetation Index IBI

    .. math ::
       IVI = \dfrac{NDBI-(SAVI+MNDWI)/2}{NDBI+(SAVI+MNDWI)/2}


    Parameters
    ----------
    ndbi : float
        Normalized Difference Built-up Index
        :math:`NDBI`
        [-]
    mndwi : float
        Modified Normalized Difference Water Index
        :math:`MNDWI`
        [-]
    savi : float
        Soil Adjusted Vegetation index
        :math:`SAVI`
        [-]

    Returns
    -------
    ivi : float
        Index based Vegetation Index
        :math:`IBI`
        [-]

    """
    return normalized_difference_index((ndbi+mndwi)/2, savi)


def urban_index(swir2, nir):
    r"""Calculate the Urban Index UI

    .. math ::
       UI = \dfrac{\rho_{SWIR\_2}-\rho_{NIR}}{\rho_{SWIR\_2}+\rho_{NIR}}


    Parameters
    ----------
    nir : float
        near infrared reflectance
        :math:`\rho_{NIR}`
        [-]
    swir2 : float
        swir reflectance (1600nm)
        :math:`\rho_{SWIR\_2}`
        [-]

    Returns
    -------
    ui : float
        Urban Index
        :math:`UI`
        [-]

    """
    return normalized_difference_index(nir, swir2)

