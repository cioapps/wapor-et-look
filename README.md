# README #

Welcome to the WaPOR version 3 documentation.

### What is this repository for? ###

* This document provides a detailed description of the processing chain applied for the production of the WaPOR version 3 data components and the theory that underlies the applied methodology, used to produce version 3 of the WaPOR database at 100m (level 2) and 30m (level 3) resolution.  
* [Wiki with description of the methodology](../../wiki/Home)  
* [Code](./ETLook/)
* For a Python package that emulates the processing chain described here, check out [pyWaPOR](https://www.fao.org/aquastat/py-wapor/).
